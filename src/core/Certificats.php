<?php

namespace core;

class Certificats
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var array
     */
    private $certificats;

    /**
     * @var array
     */
    private $map;

    /**
     * @param $path string
     *
     * @throws \Exception
     */
    public function __construct($path)
    {
        if (!is_dir($path) || (is_dir($path) && !is_readable($path))) {
            throw new \Exception("Répertoire $path introuvable ou inaccessible.");
        }

        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getCertificats()
    {
        if (null === $this->certificats) {
            $this->certificats = [];
            $this->map = [];

            if ($handle = opendir($this->getPath())) {
                while (false !== ($entry = readdir($handle))) {
                    if ('.' !== $entry && '..' !== $entry) {
                        $file = $this->getPath().'/'.$entry;

                        try {
                            $cert = new Certificat($file);
                            $this->certificats[$cert->getFilename()] = $cert;
                            $this->map[$cert->getHash()] = $cert->getFilename();
                        } catch (\Exception $e) {
                        }
                    }
                }

                closedir($handle);
            }
        }

        return $this->certificats;
    }

    /**
     * @return bool
     */
    public function hasCertificats()
    {
        return count($this->getCertificats()) > 0;
    }

    /**
     * @return array
     */
    private function getMap()
    {
        if (null === $this->map) {
            // $this->map est initialisé avec la récupération des certificats.
            $this->getCertificats();
        }

        return $this->map;
    }

    /**
     * @param $hash string
     *
     * @return Certificat|null
     */
    public function getCertificat($hash)
    {
        $result = null;

        $map = $this->getMap();
        if (array_key_exists($hash, $map)) {
            $cert = $map[$hash];
            $certs = $this->getCertificats();
            if (array_key_exists($cert, $certs)) {
                $result = $certs[$cert];
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function hasRequestedParameter()
    {
        return is_array($_GET)
            && array_key_exists(Certificat::GET_KEY, $_GET)
            && is_string($_GET[Certificat::GET_KEY]);
    }

    /**
     * @return Certificat|null
     */
    public function getRequestedCertificat()
    {
        return $this->hasRequestedParameter()
            ? $this->getCertificat(trim($_GET[Certificat::GET_KEY]))
            : null;
    }
}
