<?php

namespace core;

class Certificat
{
    const SALT = '4tgfRTh&_"a-7@)="rf%';
    const GET_KEY = 'k';

    /**
     * @var string
     */
    private $file;

    /**
     * @param $path file
     *
     * @throws \Exception
     */
    public function __construct($file)
    {
        if (!is_file($file) || (is_file($file) && !is_readable($file))) {
            throw new \Exception("Fichier $file introuvable ou inaccessible.");
        }

        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return basename($this->getFile());
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return file_get_contents($this->getFile());
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return md5(serialize(['name' => $this->getFilename(), 'salt' => self::SALT]));
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME']
            .'?'.self::GET_KEY.'='.$this->getHash();
    }

    public function printContent()
    {
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Expires: Sat, 7 Jul 2012 00:00:00 GMT');
        header('Pragma: no-cache');
        header('Content-Type: text/plain');
        print_r($this->getContent());
        exit(0);
    }

    public function downloadContent()
    {
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Expires: Sat, 7 Jul 2012 00:00:00 GMT');
        header('Pragma: no-cache');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: Binary');
        header('Content-disposition: attachment; filename="'.$this->getFilename().'"');
        readfile($this->getFile());
        exit(0);
    }
}
