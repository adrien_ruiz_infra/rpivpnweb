<?php

// Chargement des composants.

require_once __DIR__.'/../vendor/autoload.php';

// Defines.

define('ENV_PATH', 'CERT_PATH');

// Fonctions.

/**
 * @param $message string|null
 */
function print404($message = null)
{
    if (is_string($message)) {
        print_r($message);
    }

    header('HTTP/1.0 404 Not Found');
    exit(1);
}

// Chargement du fichier de configuration.

try {
    $env = __DIR__.'/../.env';

    try {
        $dotenv = new \Symfony\Component\Dotenv\Dotenv();
        $dotenv->load($env);
    } catch (\Exception $e) {
        throw new \Exception("Impossible de lire le fichier '$env'.");
    }

    if (!array_key_exists(ENV_PATH, $_ENV)) {
        throw new \Exception("La clé '".ENV_PATH."' n'existe pas dans lire le fichier '$env'.");
    }
} catch (\Exception $e) {
    print404($e->getMessage());
}

// Démarrage de l'application.

try {
    $certs = new \core\Certificats($_ENV[ENV_PATH]);
} catch (\Exception $e) {
    print404($e->getMessage());
}

if ($certs->hasRequestedParameter()) {
    $cert = $certs->getRequestedCertificat();
    if (null === $cert) {
        print404();
    } else {
        $cert->downloadContent();
    }
}
