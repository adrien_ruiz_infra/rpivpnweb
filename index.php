<?php
    require_once 'src/bootstrap.php';
    /* @var $certs \core\Certificats */
?>
<!doctype html>
<html lang="fr">
    <head>
        <title>Certificats VPN</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body class="bg-light">
        <div class="container">
            <main>
                <div class="pt-5 text-center">
                    <h2>🛡️ Certificats VPN par utilisateur</h2>
                </div>
                <div class="py-5">
                    <table class="table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Certificats OpenVPN</th>
                            </tr>
                        </thead>
                        <?php if ($certs->hasCertificats()) { ?>
                            <tbody>
                                <?php foreach ($certs->getCertificats() as $index => $cert) { ?>
                                    <tr>
                                        <td scope="row"><?php echo $cert->getFilename(); ?></td>
                                        <td>
                                            <a href="<?php echo $cert->getUrl(); ?>">Télécharger le certificat</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        <?php } ?>
                    </table>
                </div>
            </main>
        </div>
    </body>
</html>